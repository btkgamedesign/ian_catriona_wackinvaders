﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public Settings settings;
	public float leftLimit;
	public float rightLimit;

	void Update()
	{
		// move your ship right
		if (Input.GetKey(KeyCode.RightArrow))
		{
			transform.Translate(new Vector3(settings.speedFactor * Time.deltaTime, 0, 0));
		}

		// move your ship left
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			transform.Translate(new Vector3(-settings.speedFactor * Time.deltaTime, 0, 0));
		}

		// Shoots them lasers
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Bullet b = BulletPool.Instance.GetBullet();
			b.transform.position = transform.position + Vector3.up;
			b.Shoot(gameObject, Vector3.up);
			GetComponent<AudioSource>().Play();
		}

			// Keeps the ship in bounds
			float newX = Mathf.Clamp(transform.position.x, leftLimit, rightLimit);
		if (transform.position.x != newX)
		{
			transform.position = new Vector3(newX, transform.position.y, transform.position.z);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		Bullet b = other.gameObject.GetComponent<Bullet>();
		if (b != null)
		{
			//intersetc with a bullet
			gameObject.SetActive(false);
			b.Reset();

			// Lil' baby man, it's over
			StateManager.Instance.LostLife();
		} else
		{
			Invader i = other.gameObject.GetComponent<Invader>();
			if (i != null)
			{
				Debug.Log("Game Over - You crashed into and invader, dummy");
				StateManager.Instance.Loss();
			}
		}
	}
}
