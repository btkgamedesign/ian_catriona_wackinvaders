﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButtonFunction : MonoBehaviour
{
    public void PlayButtonClicked()
    {
        Debug.Log("The Play button has been pressed! Switching to Main.");
        SceneManager.LoadScene("Main");
        GetComponent<AudioSource>().Play();
    }
}
