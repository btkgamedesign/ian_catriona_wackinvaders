﻿using UnityEngine;
using UnityEngine.UI;
public class MenuShip : MonoBehaviour
{
    public Button yourButton;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        Debug.Log("You destroyed the ship!");
        GetComponent<AudioSource>().Play();
        GetComponent<Image>().enabled = false;
    }
}
