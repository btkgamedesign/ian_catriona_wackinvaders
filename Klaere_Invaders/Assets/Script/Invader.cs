﻿using UnityEngine;
using DG.Tweening;

public class Invader : MonoBehaviour
{
    static int killed;
    public Settings settings;
    float leftLimit;
    float rightLimit;
    bool goingRight;
 
    void Start()
    {
        leftLimit = transform.position.x - settings.invaderMoveRange;
        rightLimit = transform.position.x + settings.invaderMoveRange;
        goingRight = true;
    }

    void Flip()
    {
        goingRight = !goingRight;
        //go down
        transform.Translate(0, -0.5f, 0);
    }

    void Update()
    {
        float speedMultiplier = 1 + settings.invaderSpeedMultiplierFactor * killed / settings.startingInvaders;

        //movement
        float moveX = 0;
        if (goingRight)
        {
            //right movement
            if(transform.position.x > rightLimit)
            {
                Flip();
            } else
            {
                moveX = speedMultiplier * settings.invaderSpeedFactor * Time.deltaTime;
            }
        } else
        {
            //left movement
            if(transform.position.x < leftLimit)
            {
                Flip();
            }else
            {
                moveX = speedMultiplier * - settings.invaderSpeedFactor * Time.deltaTime;
            }
        }

        transform.Translate(moveX, 0, 0);

        // Shoot at random
        if (Random.value < settings.invaderShotChancePerSecond * Time.deltaTime)
        {
            int layerMask = 1 << LayerMask.NameToLayer("invader");
            bool hasInvaderUnderMe = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), 10, layerMask);
            if(!hasInvaderUnderMe)
            {
                // only if there are no invaders underneath me*
                Bullet b = BulletPool.Instance.GetBullet();
                b.transform.position = transform.position + Vector3.down;
                b.Shoot(gameObject, Vector3.down);
                GetComponent<AudioSource>().Play();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        // check if we collided with a bullet that wasn't shot by us, god pls work bruh.
        if (b != null && b.shooter != gameObject)
        {
            killed++;
            Score.Instance.AddToScore(1);

            //intersetc with a bullet
            gameObject.SetActive(false);
            b.Reset();

            //play effect
            ParticleSystem e = EffectPool.Instance.GetEffect();
            e.transform.position = transform.position;
            e.Play();

            //return after playing
            DOVirtual.DelayedCall(1,
                () => { 
                    //CLEANUP
                    EffectPool.Instance.ReturnEffect(e.gameObject);

                    // check for victory
                    if(killed >= settings.startingInvaders)
                    {
                        killed = 0;
                        StateManager.Instance.Victory();
                    }
                }
                );
        }
    }
}
