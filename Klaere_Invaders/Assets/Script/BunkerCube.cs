﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunkerCube : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        {
            //intersetc with a bullet
            transform.position = new Vector3(300, 0, 300);
            b.Reset();
            GetComponent<AudioSource>().Play();
        }
    }
}
