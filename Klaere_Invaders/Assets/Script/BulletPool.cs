﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    public static BulletPool Instance;
    public Settings settings;

    public GameObject bulletPrefab;
    public List<Bullet> bullets;

    public int howManyBullets;
    public int lines;

    void Start()
    {
        Instance = this;

        bullets = new List<Bullet>();
        int bulletsPerLine = howManyBullets / lines;

        int bulletId = 0;
        for (int x = 0; x < bulletsPerLine; x++)
        {
            for (int y = 0; y < lines; y++)
            {
                GameObject bullet = Instantiate(bulletPrefab, transform);
                bullets.Add(bullet.GetComponent<Bullet>());

                bullet.transform.localPosition = new Vector3(
                   x - bulletsPerLine / 2f + 0.5f, -y, 0);

                bullet.name = $"bullet {bulletId}";
                bulletId++;
            }
        }
    }

    public Bullet GetBullet()
    {
        Bullet b = bullets[0];
        bullets.RemoveAt(0);
        return b;
    }
    public void ReturnBullet(Bullet b)
    {
        bullets.Add(b);
    }
}
