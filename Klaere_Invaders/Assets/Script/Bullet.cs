﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Settings settings;
    Vector3 originalPosition;
    public GameObject shooter;

    void Start()
    {
        originalPosition = transform.position;
    }

    public void Shoot(GameObject s,Vector3 up)
    {
        shooter = s;
        GetComponent<Rigidbody>().velocity = up * settings.bulletSpeed;
    }

    public void Reset()
    {
        shooter = null;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        transform.position = originalPosition;
        BulletPool.Instance.ReturnBullet(this);
    }
}
