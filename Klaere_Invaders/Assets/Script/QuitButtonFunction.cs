﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButtonFunction : MonoBehaviour
{
    public void QuitButtonClicked()
    {
        Debug.Log("The Quit button has been pressed! Game should quit.");
        Application.Quit();
        GetComponent<AudioSource>().Play();
    }
}
